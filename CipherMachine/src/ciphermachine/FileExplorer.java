package ciphermachine;


import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 *
 * @author James Wienold
 */
public class FileExplorer {

    private static String filePath;
    public FileExplorer() {
        
    }

    /**
     * This method opens up a file chooser
     *
     * With the file chooser it is converted all the way down to a string
     *
     * @return
     * 
     * @throws java.io.IOException
     */
    public static String getTextFile() throws IOException {
        JFileChooser fileSelector = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt", "txt");
        fileSelector.setFileFilter(filter);
        fileSelector.showOpenDialog(null);
        String fileInput = null;
        try {
            List<String> fileList = Files.readAllLines(Paths.get(fileSelector.getSelectedFile().getAbsolutePath()));
            Object[] fileAsArray = fileList.toArray();
            fileInput = Arrays.toString(fileAsArray);
        } catch (IOException ex) {
            Logger.getLogger(EnigmaGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        filePath = fileSelector.getSelectedFile().getAbsolutePath();
        return fileInput;
    }

    public static void saveFile(String messageToFile) throws IOException {

        final Path path = Paths.get(filePath);

        try (
            final BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.CREATE);) {
            writer.write(messageToFile);
            writer.flush();
        }
    }
}
