package ciphermachine;

/**
 *
 * @author James Wienold
 */
public class CipherDriver {

    public Cipher getCaesarCipher() {
        return new CaesarCipher();
    }

    public Cipher getSubstitutionCipher() {
        return new SubstitutionCipher();
    }
    
    public Cipher getRotatingCipher() {
        return new RotatingCipher();
    }
}
