package ciphermachine;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
/**
 *
 * @author James Wienold
 */
public class EnigmaGUI extends JFrame {

    GridBagConstraints location = new GridBagConstraints();
    JRadioButton encode = new JRadioButton("Encode");
    JRadioButton ceasarCipher = new JRadioButton("Ceasar Cipher");

    JRadioButton fromFile = new JRadioButton("From File");
    JRadioButton fromKeyboard = new JRadioButton("Keyboard");

    JRadioButton substitutionCipher = new JRadioButton("Substitution Cipher");
    JRadioButton rotatingCipher = new JRadioButton("Rotating Cipher");
    JRadioButton decode = new JRadioButton("Decode");

    Icon header = new ImageIcon(getClass().getResource("../resources/images/enigmaPictureHeader.jpg"));

    JButton submitButton;

    final ButtonGroup typeOfCipher = new ButtonGroup();
    final ButtonGroup typeOfCode = new ButtonGroup();
    final ButtonGroup typeOfInput = new ButtonGroup();

    public JTextField usersSub;
    public String subbedText;

    private CipherDriver cipherDriver;
    private Cipher pickedCipher;

    private SpyMusic spy;
    private AudioInputStream ais = null;
    private Clip musicClip = null;
    private String keyboardInput;

    public EnigmaGUI() {
        //setTitle("James' Beautiful Enigma Machine");
        super("Beautiful Enigma Machine");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900, 600);
        setLocationRelativeTo(null);
        //This a placeholder image. (North side of the Border)
        setLayout(new GridBagLayout());
        JLabel enigmaLabelPanel = new JLabel(header);

        //User options in a panel.
        JPanel userOptions = new JPanel();
        userOptions.setLayout(new GridBagLayout());
        GridBagConstraints panelLocation = new GridBagConstraints();

        JLabel codeLabel = new JLabel("Encode/Decode");
        JLabel whatInputType = new JLabel("What Input Type");
        JLabel subCode = new JLabel("Please type in your substitution code for the standard alphabet");
        //Default selection
        ceasarCipher.setSelected(true);
        fromKeyboard.setSelected(true);
        encode.setSelected(true);
        //Sets focusable to off
        ceasarCipher.setFocusable(false);
        substitutionCipher.setFocusable(false);
        rotatingCipher.setFocusable(false);
        fromFile.setFocusable(false);
        fromKeyboard.setFocusable(false);
        encode.setFocusable(false);
        decode.setFocusable(false);

        //Sets the buttons in certain locations on the panel (ipad spaces out cols)
        panelLocation.ipadx = 50;
        panelLocation.gridx = 0;
        panelLocation.gridy = 0;
        userOptions.add(codeLabel, panelLocation);
        panelLocation.gridy++;
        userOptions.add(encode, panelLocation);
        panelLocation.gridy++;
        userOptions.add(decode, panelLocation);
        panelLocation.gridx = 2000;
        panelLocation.gridy = 1;
        userOptions.add(ceasarCipher, panelLocation);
        panelLocation.gridy++;
        userOptions.add(substitutionCipher, panelLocation);
        panelLocation.gridy++;
        userOptions.add(rotatingCipher, panelLocation);
        panelLocation.gridx = 4900;
        panelLocation.gridy = 0;
        userOptions.add(whatInputType, panelLocation);
        panelLocation.gridy++;
        userOptions.add(fromKeyboard, panelLocation);
        panelLocation.gridy++;
        userOptions.add(fromFile, panelLocation);

        JPanel subInput = new JPanel();
        subInput.add(subCode);

        //Adds to ButtonGroup 
        typeOfInput.add(fromFile);
        typeOfInput.add(fromKeyboard);
        //Adds to ButtonGroup
        typeOfCipher.add(ceasarCipher);
        typeOfCipher.add(substitutionCipher);
        typeOfCipher.add(rotatingCipher);
        //Adds to ButtonGroup
        typeOfCode.add(encode);
        typeOfCode.add(decode);

        //JButton for submission.
        submitButton = new JButton("SUBMIT");
        ButtonHandler handler = new ButtonHandler();
        submitButton.addActionListener(handler);
        //Can use enter to submit instead of clicking 
        this.getRootPane().setDefaultButton(submitButton);

        usersSub = new JTextField(26);
        location.gridx = 0;
        location.gridy = 0;
        add(enigmaLabelPanel, location);
        location.gridy = 1;
        add(userOptions, location);
        location.gridy++;
        add(subInput, location);
        location.gridy++;
        add(usersSub, location);
        location.gridy++;
        add(submitButton, location);
        
        musicStart("../resources/music/Fallout_3_-_Main_Theme.wav");

        setVisible(true);
    }

    
    
     public void musicStart(String fileName) {
        
        try {
                musicClip = AudioSystem.getClip();
                ais = AudioSystem.getAudioInputStream(getClass().getResource(fileName));
                musicClip.open(ais);
            } catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
                //empty
            }
            //musicClip.start();
            
            musicClip.loop(100);
    }
     
    private class ButtonHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            //Listening to my selection
            subbedText = usersSub.getText();
            //Finding which type of cipher is selected.
            if (rotatingCipher.isSelected()) {
                cipherDriver = new CipherDriver();
                pickedCipher = cipherDriver.getRotatingCipher();
            } else if (ceasarCipher.isSelected()) {
                cipherDriver = new CipherDriver();
                pickedCipher = cipherDriver.getCaesarCipher();
            } else if (substitutionCipher.isSelected()) {
                cipherDriver = new CipherDriver();
                pickedCipher = cipherDriver.getSubstitutionCipher();
            }
            //Finding which type of input is selected.
            if (fromFile.isSelected()) {
                try {
                    keyboardInput = FileExplorer.getTextFile();
                } catch (IOException ex) {
                    Logger.getLogger(EnigmaGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (fromKeyboard.isSelected()) {
                keyboardInput = JOptionPane.showInputDialog("Input your message");
            }
            //Finding which type of code is selected.
            if (encode.isSelected()) {
                if (fromFile.isSelected()) {
                    try {
                        FileExplorer.saveFile(pickedCipher.encode(keyboardInput, subbedText));
                    } catch (IOException ex) {
                        Logger.getLogger(EnigmaGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    JOptionPane.showMessageDialog(null, "Your message has been encoded.");
                } else {
                    JOptionPane.showMessageDialog(null, pickedCipher.encode(keyboardInput, subbedText));
                }
            } else if (decode.isSelected()) {
                if (fromFile.isSelected()) {
                    try {
                        FileExplorer.saveFile(pickedCipher.decode(keyboardInput, subbedText));
                    } catch (IOException ex) {
                        Logger.getLogger(EnigmaGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    JOptionPane.showMessageDialog(null, "Your message has been decoded.");
                } else {
                    JOptionPane.showMessageDialog(null, pickedCipher.decode(keyboardInput, subbedText));
                }

            }
        }
    }
}
