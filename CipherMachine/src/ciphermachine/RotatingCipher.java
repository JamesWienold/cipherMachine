package ciphermachine;

import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author James Wienold
 */
public class RotatingCipher extends Cipher {

    private Map<Character, Character> encodeSubstitution;
    private Map<Character, Character> decodeSubstitution;
    char[] charsInString;
    private String newKey;

    /**
     * Encodes a string with the Rotating encryption
     *
     * @param stringToEncode The string that will be encoded
     * @param key The substitution key 
     * 
     * @return The encoded string
     */
    @Override
    public String encode(String stringToEncode, String key) {
        if (key.length() != 26) {
            throw new IllegalArgumentException();
        }
        newKey = key;
        encodeSubstitution = new HashMap<>();
        char[] chars = key.toLowerCase().toCharArray();
        for (int n = 0; n < 26; n++) {
            encodeSubstitution.put((char) ('a' + n), chars[n]);
        }

        String encodedString = "";
        for (char character : stringToEncode.toLowerCase().toCharArray()) {

            if (Character.isAlphabetic(character)) {
                if (character > 'z') {
                    character -= 26;
                }
                character = encodeSubstitution.get(character);
            }
            encodeSubstitution = rotateEncode(encodeSubstitution, newKey);
            encodedString += character;

        }
        return encodedString;
    }
    /**
     * Decodes a string encoded with a Rotating key
     *
     * @param stringToDecode the String that will be decoded
     * @param key The key used to encode the string 
     * 
     * @return The decoded string
     */
    @Override
    public String decode(String stringToDecode, String key) {
        if (key.length() != 26) {
            throw new IllegalArgumentException();
        }
        newKey = key;
        decodeSubstitution = new HashMap<>();
        char[] chars = key.toLowerCase().toCharArray();
        for (int n = 0; n < 26; n++) {
            decodeSubstitution.put(chars[n], (char) ('a' + n));
        }

        String decodedString = "";
        for (char character : stringToDecode.toLowerCase().toCharArray()) {

            if (Character.isAlphabetic(character)) {
                if (character > 'z') {
                    character -= 26;
                }
                character = decodeSubstitution.get(character);
            }
            decodeSubstitution = rotateDecode(decodeSubstitution, newKey);
            decodedString += character;

        }
        return decodedString;
    }

    
    public Map<Character, Character> rotateEncode(Map codedSubstitution, String key) {
        String newKeys = "";
        for (char character : key.toLowerCase().toCharArray()) {
            character = (char) (character + 1);
            if (character > 'z') {
                character -= 26;
            }
            newKeys += character;
        }
        char[] chars = newKeys.toLowerCase().toCharArray();
        for (int n = 0; n < 26; n++) {
            codedSubstitution.put((char) ('a' + n), chars[n]);
        }
        newKey = newKeys;
        return codedSubstitution;
    }

    public Map<Character, Character> rotateDecode(Map codedSubstitution, String key) {
        String newKeys = "";
        for (char character : key.toLowerCase().toCharArray()) {
            character = (char) (character + 1);
            if (character > 'z') {
                character -= 26;
            }
            newKeys += character;
        }
        char[] chars = newKeys.toLowerCase().toCharArray();
        for (int n = 0; n < 26; n++) {
            codedSubstitution.put(chars[n], (char) ('a' + n));
        }
        newKey = newKeys;
        return codedSubstitution;
    }
}
