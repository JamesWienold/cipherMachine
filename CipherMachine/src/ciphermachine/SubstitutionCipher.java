package ciphermachine;

import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author James Wienold
 */
public class SubstitutionCipher extends Cipher {
    private Map<Character, Character> encodeSubstitution;
    private Map<Character, Character> decodeSubstitution;
    /**
     * Encodes a string with the substitution encryption
     * 
     * @param stringToEncode
     *            The string that will be encoded
     * @param key
     *            The substitution key
     * @return The encoded string
     */
    @Override
    public String encode(String stringToEncode, String key) {
        if (key.length() != 26) {
            throw new IllegalArgumentException();
        }
        encodeSubstitution = new HashMap<>();
        char[] chars = key.toLowerCase().toCharArray();
        for (int n = 0; n < 26; n++) {
            encodeSubstitution.put((char) ('a' + n), chars[n]);
        }
        
        String encodedString = "";
        for (char character : stringToEncode.toLowerCase().toCharArray()) {
            if (Character.isAlphabetic(character)) {
                character = encodeSubstitution.get(character);
            }
            encodedString += character;
        }
        return encodedString;
    }
    /**
     * Decodes a string encoded with a substitution key
     * 
     * @param stringToDecode
     *            the String that will be decoded
     * @param key
     *            The key used to encode the string
     * @return The decoded string
     */
    @Override
    public String decode(String stringToDecode, String key) {
        if (key.length() != 26) {
            throw new IllegalArgumentException();
        }
        decodeSubstitution = new HashMap<>();
        char[] chars = key.toLowerCase().toCharArray();
        for (int n = 0; n < 26; n++) {
            decodeSubstitution.put(chars[n],  (char) ('a' + n));
        }

        String decodedString = "";
        for (char character : stringToDecode.toLowerCase().toCharArray()) {
            if (Character.isAlphabetic(character)) {
                character = decodeSubstitution.get(character);
            }
            decodedString += character;
        }
        return decodedString;
    }
}
