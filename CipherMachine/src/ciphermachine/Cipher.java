
package ciphermachine;

/**
 *
 * @author James Wienold
 */
public abstract class Cipher {
    public abstract String encode(String stringToEncode, String key);
    public abstract String decode(String stringToDecode, String key);
}
