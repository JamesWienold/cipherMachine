package ciphermachine;
/**
 *
 * @author James Wienold
 */
public class CaesarCipher extends Cipher {
    /**
     * Encodes a string with the Caesar encryption
     * 
     * @param stringToEncode
     *            The string that will be encoded
     * @param key
     *            The substitution key (inputting null for CaesarCipher)
     * @return The encoded string
     */   
    @Override
    public String encode(String stringToEncode, String key) {
        String encodedMessage = "";
        char[] charsInString = stringToEncode.toLowerCase().toCharArray();
        for (char character : charsInString) {
            if (Character.isAlphabetic(character)) {
                character += 3;
                if (character > 'z') {
                    character -= 26;
                }            
            }
            encodedMessage += character;
        }
        return encodedMessage;
    }
    /**
     * Decodes a string encoded with a Caesar key
     * 
     * @param stringToDecode
     *            the String that will be decoded
     * @param key
     *            The key used to decode the string (inputting null for CaesarCipher)
     * @return The decoded string
     */
    @Override 
    public String decode(String stringToDecode, String key) {
        String decodedMessage = "";
        char[] charsInString = stringToDecode.toLowerCase().toCharArray();
        for (char character : charsInString) {           
            if (Character.isAlphabetic(character)) {
                character -= 3;
                if (character < 'a') {
                    character += 26;               
                }
            }
            decodedMessage += character;
        }
        return decodedMessage;
    }
}
