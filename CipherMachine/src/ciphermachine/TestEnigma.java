package ciphermachine;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
/**
 *
 * @author James Wienold
 */
public class TestEnigma {

    public TestEnigma() {
    }
    
    public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> new EnigmaGUI());
//    try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {            
//        }
    }
}
