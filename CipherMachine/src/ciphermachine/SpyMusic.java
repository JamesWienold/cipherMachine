
package ciphermachine;

import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author James Wienold
 */
public class SpyMusic {
    private AudioInputStream ais = null;
    private Clip musicClip = null;
    
     public void musicStart(String fileName) {
        
        try {
                musicClip = AudioSystem.getClip();
                ais = AudioSystem.getAudioInputStream(getClass().getResource(fileName));
                musicClip.open(ais);
            } catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
                //empty
            }
            musicClip.start();
            
            //musicClip.loop(0);
    }
}
